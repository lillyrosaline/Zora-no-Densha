#!/bin/sh
#
#=======================================================================================
#
#           FILE:   git-update-version
#
#    DESCRIPTION:   Retrieves current semantic version information from the repository
#                    in the (parent) directory of this script file and prints via echo.
#
#         AUTHOR:   Leshuwa Kaiheiwa
#   ORGANISATION:   ZnDevelopment
#        VERSION:   1.1
#        CREATED:   2021/03/03 12:20:50
#       REVISION:   2021/03/14 18:50:30
#
#          NOTES:   Searches for the most recent valid Git tag (see below) in the branch
#                    history and determines whether the current build is stable.
#                   Builds are considered 'stable' if the HEAD that is built from
#                    1) contains no changed files and
#                    2) is tagged with a valid Git tag.
#
#                   Git tags are considered valid if they match following format:
#
#                      v<MOD_VERSION>-<FORGE_VERSION>
#
#                    where
#
#                      MOD_VERSION   is the mod's manually assigned X.Y.Z version with
#                                     major, minor, and patch levels, respectively.
#
#                      FORGE_VERSION is the supported Forge version; note the absence
#                                     of the respective Minecraft version here.
#
#                   If the recognised build version is not considered stable, it will
#                    receive a 'SNAPSHOT'-suffix along with the build revision (i.e.
#                    the number of commits in the current branch since the previous
#                    valid tag in branch history).
#
#     EXIT CODES:   0 - Successful termination.
#                   1 - Error during execution of a command.
#                   2 - No tags in history to use for 'git describe' matcher blob.
#                 127 - Git is either not installed or not available.
# 
#=======================================================================================



# Construct regular expression for valid Git tags.
          NUM_REGEX="(0|([1-9][0-9]*))"
  MOD_VERSION_REGEX="$NUM_REGEX\.$NUM_REGEX\.$NUM_REGEX"
FORGE_VERSION_REGEX="$NUM_REGEX\.$NUM_REGEX\.$NUM_REGEX(\.$NUM_REGEX)?"
    VALID_TAG_REGEX="^v$MOD_VERSION_REGEX-$FORGE_VERSION_REGEX"

# Prepare and clear log file before starting.
log_file_dir="logs"
log_file_path="$log_file_dir/$(basename "$0").log"

if [[ ! -d "$log_file_dir" ]]; then
	mkdir "$log_file_dir"
fi

echo "" > "$log_file_path"



#---------------------------------------------------------------------------------------
# Ensure git is installed, in case someone executes this script manually..
#---------------------------------------------------------------------------------------
git_version="$(git version)"
if [[ -z "$git_version" || $? -ne 0 ]]; then
	echo "[ERROR] Git version check failed with \"$git_version\" ($?)." >> "$log_file_path"
	echo "[ERROR] Do you have Git installed and in your PATH?" >> "$log_file_path"
	exit 127 # Exiting with "Command not found" error code.
fi



#---------------------------------------------------------------------------------------
# List all modified files in the repository and count the number of lines.
#---------------------------------------------------------------------------------------

changed_files="$(git status --porcelain | wc -l)"
if [[ $? -ne 0 ]]; then
	echo "[ERROR] Couldn't retrieve Git status ($?)." >> "$log_file_path"
	exit 1
fi



#---------------------------------------------------------------------------------------
# Grab the most recent tag's name from the current branch.
#---------------------------------------------------------------------------------------

version=$(
	for tag in $(git tag | grep -E "$VALID_TAG_REGEX"); do
		git describe --tags --long --match="$tag" 2>/dev/null
	done | 
		sort -k2 -t"-" | 
		head -n 1 | 
		cut -d"-" -f-2
)

if [[ $? -ne 0 ]]; then
	echo "[ERROR] Couldn't retrieve Git tags ($?)." >> "$log_file_path"
	exit 1
elif [[ -z "$version" ]]; then
	echo "[ERROR] No tags found at for current HEAD." >> "$log_file_path"
	exit 2
fi



#---------------------------------------------------------------------------------------
# Generate build number for version string, in case we need it.
#---------------------------------------------------------------------------------------

# Store current branch for 'git rev-list' command.
current_branch="$(git branch --show-current)"
if [[ $? -ne 0 ]]; then
	echo "[ERROR] Couldn't retrieve Git branch ($?)." >> "$log_file_path"
	exit 1
fi

# Generate build number.
if [[ -z "$current_branch" ]]; then

	# Special case: HEAD is detached (i.e. current branch is empty)
	current_branch="<Detached>"
	build_num=1

else

	# Standard case: generate build number from number of commits in branch.
	build_num="$(git rev-list --count "$current_branch" HEAD)"
	if [[ $? -ne 0 ]]; then
		exit 1
	fi

fi



#---------------------------------------------------------------------------------------
# Determine whether to append a suffix to the version number and print result.
#---------------------------------------------------------------------------------------

# Retrieve tags at current commit.
head_tags=$(git tag --points-at HEAD)
if [[ $? -ne 0 ]]; then
	echo "[ERROR] Couldn't retrieve tags for HEAD ($?)." >> "$log_file_path"
	exit 1
fi

# If this commit is tagged, check whether it contains the selected tag as well.
is_stable=0
if [[ ! -z "$head_tags" ]]; then
	for tag in $head_tags; do
		if [[ "$tag" == "$version" ]]; then
			is_stable=1
			break
		fi
	done
fi

# Append 'SNAPSHOT'-suffix for unstable or modified builds.
if [[ $is_stable -ne 1 || $changed_files -ne 0 ]]; then
	version="$version-SNAPSHOT$build_num"
fi

# Print result and terminate with success.
echo "Version \"$version\" on branch \"$current_branch\"." >> "$log_file_path"
echo "$version"
exit 0
